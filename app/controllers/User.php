<?php
class User extends Controller
{
    public function index()
    {
        $data["title"] = "Asu_Kasen";
        $data["user"] = $this->model("User_model")->show_profile($_SESSION);
        $data["company"] = "Asu_Kasen";

        $this->view('templates/header', $data);
        $this->view('templates/navbar', $data);
        $this->view('user/profile', $data);
        $this->view('templates/footer');
    }
}
